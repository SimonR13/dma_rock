package de.dma.sayhello;

import java.util.Locale;

import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class StartActivity extends ActionBarActivity implements OnInitListener
{
    private static TextToSpeech tts;
    private static Locale loc = Locale.GERMAN;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        
        tts = new TextToSpeech(this, (OnInitListener) this);

        Button btnRead = (Button) findViewById(R.id.button1);
        btnRead.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                TextView tv = (TextView) findViewById(R.id.editText1);
                tts.setLanguage(loc);
                tts.speak(tv.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        
    }
    
    public void onClickRadioLocale(View v) {
        switch(v.getId()) {
            case (R.id.radio0):
                loc = Locale.GERMAN;
                break;
            case (R.id.radio1):
                loc = Locale.ENGLISH;
                break;
            case (R.id.radio2):
                loc = Locale.FRENCH;
                break;
            default:
                loc = Locale.KOREAN;
        }
    }
    
    public void onInit(int status) {
        System.out.println("onInit()");
    }

}
