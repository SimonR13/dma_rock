package de.dma.einkaufsliste;

import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;
import android.os.Bundle;

public class MainActivity extends ActionBarActivity
{

    private final int[] ids = { R.id.AutoCompleteTextView00, R.id.AutoCompleteTextView01, R.id.AutoCompleteTextView02,
        R.id.AutoCompleteTextView03, R.id.AutoCompleteTextView04 };
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        final String[] listItems = getResources().getStringArray(R.array.ListItems);;
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i = 0; i < ids.length; i++) {
            AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(ids[i]);
            actv.setAdapter(arrayAdapter);
        }
        
        
        Button btnDone = (Button) findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                StringBuilder toastBuilder = new StringBuilder();
                for (int i = 0; i < ids.length; i++) {
                    AutoCompleteTextView txt = (AutoCompleteTextView) findViewById(ids[i]);
                    String tmp = txt.getText().toString();
                    if (!tmp.equals("")) {
                        toastBuilder.append(tmp);
                        toastBuilder.append(", ");
                    }
                    System.out.println(txt.toString() + "; " + tmp + "; " + toastBuilder.toString());
                    
                }
                Toast.makeText(MainActivity.this, toastBuilder.toString(), Toast.LENGTH_LONG).show();
            }
        });
        
    }

}
