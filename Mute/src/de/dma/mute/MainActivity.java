package de.dma.mute;

import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;

public class MainActivity extends ActionBarActivity
{

    private AudioManager mAudioManager; 
    private Boolean BOOL_MUTE;
    private Drawable DRAWABLE_MUTE;
    private Drawable DRAWABLE_UNMUTE;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (DRAWABLE_MUTE == null) {
            DRAWABLE_MUTE = (Drawable) getResources().getDrawable(R.drawable.ic_device_access_volume_on);
        }
        if (DRAWABLE_UNMUTE == null) {
            DRAWABLE_UNMUTE = (Drawable) getResources().getDrawable(R.drawable.ic_device_access_volume_muted);
        }
        if (mAudioManager == null){
            mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        }
        
        final Button btn = (Button) findViewById(R.id.button1);
        final ImageButton imgBtn = (ImageButton) findViewById(R.id.imageButton1);
        
        if (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
            setIcons(imgBtn, btn, false);
        }
        else {
            setIcons(imgBtn, btn, true);

        }
        
        imgBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                toggleMute();
                setIcons(imgBtn, btn, !BOOL_MUTE);
            }
        });
        btn.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                toggleMute();
                setIcons(imgBtn, btn, !BOOL_MUTE);
            }
        });
    }
    
    private void setIcons(final ImageButton imgBtn, final Button btn, final Boolean mute) {
        
        if (mute) {
            imgBtn.setImageDrawable(DRAWABLE_UNMUTE);
            btn.setText(R.string.btnToggleUnMute);
            BOOL_MUTE = mute;
        }
        else {
            imgBtn.setImageDrawable(DRAWABLE_MUTE);
            btn.setText(R.string.btnToggleMute);
            BOOL_MUTE = mute;
        }
    }
    
    private void toggleMute() {
        
        if (BOOL_MUTE) {
            mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        }
        else
        {
            mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        }
        
    }
}
